import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  todoItem:string[] = ['รายการที่ต้องทำ1','รายการที่ต้องทำ2'];

  addNewTodoItem(newTodoItem:string) {
    this.todoItem.push(newTodoItem);
  }
}
